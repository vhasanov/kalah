import java.lang.reflect.Array;
import java.util.Scanner;

public class Application {

    public static void main(String[] args) throws Exception {
        GameService gameService=new GameService();
        Game game =  gameService.newGame();

        Scanner scanner=new Scanner(System.in);
        print("Game is started. Inital player will detect after first move");
        displayBoard(game);

        // Simulation
        int[] moves = {1,2,13,6,13,12,1,13,11,1,13,12,2,6,4,11,5,9,3,8,2,1,12,1,10,2};
        for (int val:moves){
            game.move(val);
        }

        displayBoard(game);


        while (!game.isGameOver()){
            try {
                int potId = scanner.nextInt();
                game.move(potId);
                displayBoard(game);
                print("Next player is " + game.getCurrentPlayer().toString());
            } catch (Exception exp){
                print(exp.getMessage());
                displayBoard(game);
            }
        }

    }

    public static void displayBoard(Game game){
        Integer[] pits = game.getBoard().getPits();

        print();
        for (int i=12;i>6;i--){
            print(i+1,pits[i]);
        }
        eoln();

        print(14,pits[13]);

        for (int i=1;i<=6;i++){
            print();
        }

        print(7,pits[6]);

        eoln();


        print();
        for (int i=0;i<6;i++){
            print(i+1,pits[i]);
        }

        eoln();

    }

    public static void print(String message){
        System.out.println(message);
    }

    public static void print(){
        System.out.print("       ");
    }

    public static void eoln(){
        System.out.println();
    }

    public static void print(Integer key,Integer value){
        System.out.print(" "+convertToString(key)+"="+convertToString(value)+" ");
    }

    public static String convertToString(Integer value){
        return value.toString().length()==1 ? "0"+value.toString() : value.toString();
    }
}
