public class Game {
    private int id;
    private Player currentPlayer;
    private Player winner;
    private Board board;
    private GameStatus status;

    public Game(int id) {
        this.id = id;
        this.currentPlayer = Player.None;
        this.winner = Player.None;
        this.board = new Board();
        this.status = GameStatus.Ongoing;
    }

    public int getId() {
        return id;
    }

    public boolean isGameOver() {
        return getStatus() == GameStatus.Over;
    }

    public Player getWinner() {
        return this.winner;
    }

    public Player getCurrentPlayer() {
        return currentPlayer;
    }

    public GameStatus getStatus() {
        return status;
    }

    public Board getBoard() {
        return this.board;
    }

    public void move(int pitId) throws Exception {
        // check game status
        if (getStatus() == GameStatus.Over) {
            throw new Exception("Game over");
        }

        // check validity of pit id
        if ((pitId < 1 || pitId > 14) || pitId == 7 || pitId == 14) {
            throw new Exception("Invaid Pit Id");
        }

        // set initial player
        if (getCurrentPlayer() == Player.None) {
            currentPlayer = (pitId >= 1 && pitId <= 6) ? Player.First : Player.Second;
        }

        // check move validity
        if ((getCurrentPlayer() == Player.First && pitId > 6) || (getCurrentPlayer() == Player.Second && pitId < 7)) {
            throw new Exception("Invalid Move");
        }

        // check empty pit
        if (getBoard().isEmpty(pitId)) {
            throw new Exception("Pit is empty");
        }

        // move operation
        boolean oneMoreMove = false;
        int stoneCount = getBoard().getValue(pitId);
        getBoard().setValue(pitId, 0);

        while (stoneCount > 0) {
            // switch to next pit
            pitId++;

            if (pitId==15){
                pitId=1;
            }

            // skip if it is opposite store
            if (getBoard().isOppositeStore(pitId, getCurrentPlayer())) {
                pitId++;

                if (pitId==15){
                    pitId=0;
                }

                continue;
            }

            // distribute stone
            stoneCount--;
            getBoard().increaseValue(pitId);

            // continue and give one more move if it's last move on own store
            if (getBoard().isOwnStore(pitId, getCurrentPlayer()) && stoneCount == 0) {
                oneMoreMove = true;
                continue;
            }

            // if it's last move and opposite pit not empty move stones to own store
            if (stoneCount == 0 && getBoard().getValue(pitId) == 1) {
                int oppositeCount = getBoard().getOppositeValue(pitId);
                if (oppositeCount > 0) {
                    getBoard().increaseStoreValue(getCurrentPlayer(), oppositeCount + 1);
                    getBoard().setValue(pitId, 0);
                    getBoard().setOppositeValue(pitId, 0);
                }
            }

        }

        // check is the game over
        if (getBoard().isEmpty(getCurrentPlayer())) {
            Player oppositePlayer = (getCurrentPlayer() == Player.First) ? Player.Second : Player.First;
            getBoard().shiftStonesToStore(oppositePlayer);

            int currentPlayerStones = getBoard().getStoreValue(getCurrentPlayer());
            int oppositePlayerStones = getBoard().getStoreValue(oppositePlayer);

            winner = (currentPlayerStones > oppositePlayerStones) ? getCurrentPlayer() : (currentPlayerStones < oppositePlayerStones) ? oppositePlayer : Player.None;
            status = GameStatus.Over;
            return;
        }
        // change player
        if (!oneMoreMove) {
            currentPlayer = (getCurrentPlayer() == Player.First) ? Player.Second : Player.First;
        }
    }

}
