import java.util.HashMap;
import java.util.Map;

public class GameService {
    private int nextId;
    private Map<Integer,Game> games;

    public GameService(){
        nextId=0;
        games=new HashMap<>();
    }

    public Game newGame(){
        Game game=new Game(++nextId);
        games.put(game.getId(),game);
        return game;
    }

    public void destroyGame(Game game){
        games.remove(game);
    }
}
