import java.lang.reflect.Array;
import java.util.HashMap;
import java.util.Map;

public class Board {

    private static final int pitsCount = 14;
    private static final int stonePerPit = 6;

    private Map<Integer, Integer> pits;

    public Board() {
        pits=new HashMap<>();

        for (int i = 1; i <= pitsCount; i++) {
            if (i == 7 || i == 14) {
                pits.put(i, 0);
                continue;
            }
            pits.put(i, stonePerPit);
        }
    }

    public Integer[] getPits(){
        return pits.values().toArray(new Integer[pits.size()]);
    }

    public int getSize(){
        return (pitsCount-2)*stonePerPit;
    }

    public int getValue(int pitId) {
        return pits.get(pitId);
    }

    public int getStoreValue(Player player){
        int pitId = (player == Player.First) ? 7 : 14;
        return getValue(pitId);
    }

    public void setValue(int pitId, int stoneCount) {
        pits.put(pitId, stoneCount);
    }

    public void increaseValue(int pitId) {
        increaseValue(pitId, 1);
    }

    public void increaseValue(int pitId, int stoneCount) {
        setValue(pitId, getValue(pitId) + stoneCount);
    }

    public void increaseStoreValue(Player player, int stoneCount) {
        int pitId = (player == Player.First) ? 7 : 14;
        this.increaseValue(pitId, stoneCount);
    }

    public boolean isOwnStore(int pitId, Player player) {
        if (player == Player.First && pitId == 7) return true;
        if (player == Player.Second && pitId == 14) return true;
        return false;
    }

    public boolean isOppositeStore(int pitId, Player player) {
        if (player == Player.First && pitId == 14) return true;
        if (player == Player.Second && pitId == 7) return true;
        return false;
    }

    public boolean isEmpty(int pitId) {
        return getValue(pitId) == 0;
    }

    public boolean isEmpty(Player player) {
        int start=(player==Player.First) ? 1 : 8;
        int end=(player==Player.First) ? 6 : 13;

        for(int i =start;i<=end;i++){
            if(!isEmpty(i)) return false;
        }

        return true;
    }

    public int getOppositeValue(int pitId) {
        return getValue(pitsCount - pitId);
    }

    public void setOppositeValue(int pitId,int stoneCount) {
        setValue(pitsCount - pitId,stoneCount);
    }

    public void shiftStonesToStore(Player player){
        int start=(player==Player.First) ? 1 : 8;
        int end=(player==Player.First) ? 6 : 13;

        for(int i =start;i<=end;i++){
            int stoneCount = getValue(i);
            setValue(i,0);
            increaseStoreValue(player,stoneCount);
        }
    }
}
